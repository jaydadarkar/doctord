# DOCTOR-D

A Specialized Application For Appointment Booking Of Doctors Build On NodeJS And Mongo DB.

## Installation

1. Clone this repository using git clone https://jaydadarkar@bitbucket.org/jaydadarkar/doctord.git
2. npm install (Install All The Required Dependences).
3. Setup ENV. Copy contents of .env.example to .env and make necessary changes.
4. Connect To MongoDB (Replace MONGO_DATABASE_URI in the env).
5. npm run-script key:generate (Generate App Key).
6. npm start (Start Express).
7. Visit https://localhost/

## Usage

1. Patients request appointment.
2. Appointments are booked by admin with date and time.
3. Doctors will see today's appointments and assign prescription to each patient.
4. Next time the patient visits, doctor will be able to see the history of patients and assign prescription.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.

## Development
1. To Make A Controller, Hit: npm run-script make:controller --name={name_of_controller}.
2. To Make A Model, Hit: npm run-script make:model --name={name_of_model}.
3. To Make A Admin, Hit: npm run-script make:admin --name={email_of_user}.

## Author
[Jay Dadarkar](https://jaydadarkar.com/)

## Dedicated To
1. Dr. Rucha Mahesh Mantri
2. Dr. Nikhil Gokhale

## License
[MIT](https://choosealicense.com/licenses/mit/)