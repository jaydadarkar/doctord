// Initialize Application
const express = require('express');
const app = express();
const mongoose = require('mongoose');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
dotenv.config();
const cookieParser = require('cookie-parser');

// Connect Database
try {
    mongoose.connect(process.env.MONGO_DATABASE_URI,
        { 
            useNewUrlParser: true,
            useUnifiedTopology: true
        });    
} catch (error) {
    if(error.name == 'MongooseServerSelectionError') return console.log('Unable To Connect To The Database.');
    return console.log(error);
}

// Bring The Route
const route = require('./route/app.js');

// Initialize Middlewares
app.use(morgan('dev'));
app.use(bodyParser.json());
app.set('view engine', 'ejs');
app.use(cookieParser(process.env.APP_KEY));

// Hit The Routes
app.use('/', route);

// Handle CSRF Token Errors
app.use(function (err, req, res, next) {
    if (err.code !== 'EBADCSRFTOKEN') return next(err)
    res.status(403).render('errors/messages',{val_errors: 'Invalid CSRF Token.'}).clearCookie('_csrf');
});

// Listen To Requests
const port = process.env.PORT || 80;
app.listen(port, () => {
    console.log(`Node JS API Listening On PORT: ${port}`);
});