// Model Head
const mongoose = require('mongoose');

const appointmentSchema = mongoose.Schema({
    name:{
        type: String,
        required: true,
        min: 3,
        max: 40
    },
    user_id:{
        type: String,
        default: null
    },
    phone:{
        type: String,
        required: true,
        min: 10,
        max: 12
    },
    email:{
        type: String,
        required: true,
        min: 6,
        max: 100
    },
    referer_doc:{
        type: String,
        max: 40,
        min: 3
    },
    appointment_type:{
        type: String,        
        required: true,
    },
    created_at:{
        type: Date,
        default: Date.now()
    },
    updated_at:{
        type: Date,
        default: Date.now()
    }
});

module.exports = mongoose.model('Appointment_Req', appointmentSchema);