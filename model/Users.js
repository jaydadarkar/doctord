// Model Head
const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    name:{
        type: String,
        required: true,
        min: 3,
        max: 40
    },
    email:{
        type: String,
        required: true,
        min: 6,
        max: 100
    },
    phone:{
        type: String,
        required: true,
        min: 10,
        max: 13
    },
    password:{
        type: String,
        required: true,
        max: 1024,
        min: 8
    },
    user_type:{
        type: String,
        required: true
    },
    refresh_token:{
        type: String,        
    },
    reset_password_requested:{
        type: Boolean,        
    },
    reset_password_token:{
        type: String,        
    },
    reset_password_expiry:{
        type: Date,        
    },
    created_at:{
        type: Date,
        default: Date.now
    },
    updated_at:{
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Users', userSchema);