const mongoose = require('mongoose');

const appointmentSchema = mongoose.Schema({
 user_id:{
    type: String
 },
 appointment_type:{
    type: String
 },
 appointment_date_time:{
    type: Date
 },
 prescription:{
    type: String
 },
 created_at:{
  type: Date,
  default: Date.now
 },
 updated_at:{
  type: Date,
  default: Date.now
 }
});

module.exports = mongoose.model('Appointment', appointmentSchema);