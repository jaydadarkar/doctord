let email = process.env.npm_config_name;
if(email == undefined) return console.log('Email Is Missing.');
if(email == null) return console.log('Email Is Missing.');

var MongoClient = require('mongodb').MongoClient;
const dotenv = require('dotenv');
dotenv.config();
var url = process.env.MONGO_DATABASE_URL;

MongoClient.connect(url, { useUnifiedTopology: true }, function(err, db) {
  if (err) throw err;
  var dbo = db.db("doctord");
  var myquery = { email: email };
  var newvalues = { $set: {user_type: "admin"} };
  dbo.collection("users").findOne({email: email}, function(err,user){
      if(user != null){
        dbo.collection("users").updateOne(myquery, newvalues, function(err, res) {
            if (err) throw err;
            console.log(email + " is now an admin.");
            db.close();
          });
      }
      else{
          console.log("No User Found");
          db.close();
        }
  });
  
});