let controller = process.env.npm_config_name;
if(controller == undefined) return console.log('Controller Name Is Missing.');
controller = controller.toLowerCase();
const fs = require('fs');

let filename = controller + '.controller.js';
let content = "const mongoose = require('mongoose');\nconst route = require('../route/app');\nconst csrf = require('csurf');\nconst csrfProtection = csrf({ cookie: true });\nconst cookieParser = require('cookie-parser');\nconst bodyParser = require('body-parser');\nconst parseForm = bodyParser.urlencoded({ extended: false });\n\nexports.create = async (req,res,next) => {\n\n}\n\nexports.read = async (req,res,next) => {\n\n}\n\nexports.readOne = async (req,res,next) => {\n\n}\n\nexports.update = async (req,res,next) => {\n\n}\n\nexports.delete = async (req,res,next) => {\n\n}";

fs.appendFile('./controller/' + filename, content, function (err) {
    if (err) console.log(err);
    console.log(controller + " Controller Created Successfully");
});