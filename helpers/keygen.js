const fs = require('fs');
const crypto = require("crypto");

let configfile = ".env";
fs.readFile(configfile, 'utf8', function (err,data) {
   if (err) {
     return console.log(err);
   }

   let result = '';
   let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   let charactersLength = characters.length;
   for( var i = 0; i < 32; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   let result_app_key = 'APP_KEY=' + result;
   
   
   let jwt_app_key = crypto.randomBytes(16).toString('hex');
   let result_jwt_app_key = 'JWT_APP_KEY=' + jwt_app_key;
   
   var replace_this = data.replace(/JWT_APP_KEY=/g, result_jwt_app_key).replace(/APP_KEY=/g, result_app_key);
   
   fs.writeFile(configfile, replace_this, 'utf8', function (err) {
      if (!err){
         console.log("APP KEYS GENERATED SUCCESSFULLY");
      }
      else{
         return console.log(err);
      }
   });   
   
});