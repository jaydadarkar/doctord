const jwt = require('jsonwebtoken');
const cookieEncrypt = require('./cookieEncrypt');
const Users = require('../model/Users');
const {v4: uuidv4} = require('uuid');

const cookieConfig = {
  httpOnly: true,
  maxAge: 604800000,
  signed: true,
  sameSite: true
};

// Auth Middleware
module.exports = async function (req,res,next){
  let token = req.signedCookies.doctord_session;
  let refresh_token = req.signedCookies.doctord_refresh;

  if (!token) return res.status(401).render('errors/401');  
    if (token.startsWith('Bearer ')) {
        token = token.slice(7, token.length);
      }

     await jwt.verify(token, process.env.JWT_APP_KEY, async function(err, decoded) {
        if (err){
          if(req.signedCookies.doctord_refresh){
          // If JWT Expires, We will use the refresh token 
           refresh_token =  cookieEncrypt.decrypt(req.signedCookies.doctord_refresh);
           await Users.findOne({refresh_token: refresh_token}, async function(err,user){
            if(err) return console.log(err);
            if(!user) return res.status(500).render('errors/500');

            // Assign A New JWT Token
            const token = await jwt.sign(
              {
                  _id: cookieEncrypt.encrypt(String(user._id))
              },
              process.env.JWT_APP_KEY,
              {
                  expiresIn: process.env.REFRESH_JWT_INTERVAL
              }
              );
        // New Refresh Token
        refresh_result = uuidv4();
        req.user_id = String(user._id);
        res.cookie('doctord_session', token, cookieConfig).cookie('doctord_refresh', cookieEncrypt.encrypt(refresh_result), cookieConfig).status(200);
        Users.updateOne({_id: String(user._id)},{refresh_token: refresh_result}, function(err, data) {
          if(err) return console.log(err);
       });
      });

        }
        else{
          return res.status(500).render('errors/500');
        }
        }
        else{
          req.user_id = cookieEncrypt.decrypt(decoded._id);
          res.status(200);  
        }

      });
    next();
}