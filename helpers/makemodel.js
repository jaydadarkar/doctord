let modelraw = process.env.npm_config_name;
if(modelraw == undefined) return console.log('Model Name Is Missing.');
model = modelraw.toLowerCase();
modelCap = modelraw[0].toUpperCase() +  modelraw.slice(1);
const fs = require('fs');

let filename = modelCap + '.js';
let content = "const mongoose = require('mongoose');\n\nconst "+ model +"Schema = mongoose.Schema({\n created_at:{\n  type: Date,\n  default: Date.now\n },\n updated_at:{\n  type: Date,\n  default: Date.now\n }\n});\n\nmodule.exports = mongoose.model('"+ modelCap +"', "+ model +"Schema);";

fs.appendFile('./model/' + filename, content, function (err) {
    if (err) console.log(err);
    console.log(model + " Model Created Successfully");
});