const jwt = require('jsonwebtoken');
const Users = require('../model/Users');
const cookieEncrypt = require('./cookieEncrypt');
const {v4: uuidv4} = require('uuid');

const cookieConfig = {
  httpOnly: true,
  maxAge: 604800000,
  signed: true,
  sameSite: true
};

// Admin Middleware
// Similar To Auth Middleware
module.exports = async function (req,res,next){
    let token = req.signedCookies.doctord_session;
    if (!token) return res.status(401).render('errors/401');
    
    if (token.startsWith('Bearer ')) {
        token = token.slice(7, token.length);
      }

    await jwt.verify(token, process.env.JWT_APP_KEY, async function (err, decoded) {
      if (err){
        if(req.signedCookies.doctord_refresh){
         refresh_token =  cookieEncrypt.decrypt(req.signedCookies.doctord_refresh);
         await Users.findOne({refresh_token: refresh_token}, async function(err,user){
          if(err) return console.log(err);
          if(!user) return res.status(500).render('errors/500');
          // Check Admin
          if (user.user_type != 'admin') return res.status(400).render('errors/400');
  
          const token = await jwt.sign(
            {
                _id: cookieEncrypt.encrypt(String(user._id))
            },
            process.env.JWT_APP_KEY,
            {
                expiresIn: process.env.REFRESH_JWT_INTERVAL
            }
            );
      refresh_result = uuidv4();
      req.user_id = String(user._id);
      res.cookie('doctord_session', token, cookieConfig).cookie('doctord_refresh', cookieEncrypt.encrypt(refresh_result), cookieConfig).status(200);
      Users.updateOne({_id: String(user._id)},{refresh_token: refresh_result}, function(err, data) {
        if(err) return console.log(err);
     });
    });

      }
      else{
        return res.status(500).render('errors/500');
      }
      }
    else{
      let id = cookieEncrypt.decrypt(String(decoded._id));
      Users.findById(id, function (err, user) {
        if (!user) {
          return res.status(401).render('errors/401');
        }
        else if (user.user_type != 'admin') {
          return res.status(400).render('errors/400');
        }
        else{
          req.user_id = user._id;
          res.status(200);  
        }
      });
    }
  });
    next();
}

