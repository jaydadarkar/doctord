const mongoose = require('mongoose');
const controller = require('./controller');
const Users = require('../model/Users');

const route = require('../route/app');

const csrf = require('csurf');
const csrfProtection = csrf({ cookie: true });

const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const parseForm = bodyParser.urlencoded({ extended: false });

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const crypto = require("crypto");

const cookieEncrypt = require('../helpers/cookieEncrypt');
const { check, validationResult } = require('express-validator');

const cookieConfig = {
    httpOnly: true,
    maxAge: 604800000,
    signed: true,
    sameSite: true
};

// Display Appointment Requests By Patients
exports.admin = async (req,res) => {
    let action = function (err, collection) {
        collection.find({}).sort({ created_at: -1 }).toArray(function(err, data) {
            res.render('admin/index', { 
                csrfToken:  req.csrfToken(),
                appoint_req: data
             });        
        });
    };

    mongoose.connection.db.collection('appointment_reqs', action);
}

// Get All Users
exports.users = async (req,res) => {
    await Users.find({}, function(err,Users){
        if(!err){
            res.render('admin/users', {csrfToken: req.csrfToken(), users: Users});
        }
        else{
            res.render('errors/messages', {val_errors: err});
        }
    });    
    
}

// Get One User
exports.user = async (req,res) => {
    let val_errors = validationResult(req.params.id);
    if (!val_errors.isEmpty()) {
      return res.status(422).render('errors/422', {
          val_errors: val_errors
        });
    }

    let user_id = String(req.params.id);
    await Users.findOne({_id: user_id}, async function (err, User){
        if(!err){
            res.render('admin/user', {csrfToken: req.csrfToken(), user: User});
        }
        else{
            res.render('errors/messages', {val_errors: err});
        }
    });
}

// Update One User
exports.updateUser = async (req,res) => {
    let user_id = String(req.body.user_id);
    await Users.findOne({_id: user_id}, async function (err, user){
        if(!err){
            await Users.updateOne({_id: user._id},{
                name: req.body.name,
                email: req.body.email,
                phone: req.body.phone,
                updated_at: Date.now()
            },function(err){
                if(err){
                    res.render('errors/messages', {val_errors: err});
                }
                else{
                    res.render('errors/messages', {val_errors: "User Updated"});
                }
            });
        }
    });
}