const mongoose = require('mongoose');
const controller = require('./controller');
const Users = require('../model/Users');
const Appointment = require('../model/Appointment');

const route = require('../route/app');

const csrf = require('csurf');
const csrfProtection = csrf({ cookie: true });

const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const parseForm = bodyParser.urlencoded({ extended: false });

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const crypto = require("crypto");
const {v4: uuidv4} = require('uuid');

const cookieEncrypt = require('../helpers/cookieEncrypt');

const cookieConfig = {
    httpOnly: true,
    maxAge: 604800000,
    signed: true,
    sameSite: true
};

const mail = require('../helpers/mail');
const { check, validationResult } = require('express-validator');

// User Dashboard
exports.dashboard = async (req,res) => {

    const id = req.user_id;
    const user = await Users.findOne({_id: id});
    await Appointment.find({user_id: id}, function(err,appointment){
        if(!err){
            res.render('auth/dashboard', { 
                csrfToken:  req.csrfToken(),
                user_detail: user,
                appointments: appointment
             });
        }
    }).sort({ created_at: -1 });
}

// Register User Layout
exports.register = (req,res,next) => {
    if(req.signedCookies.doctord_session){
        res.redirect('/dashboard');
    }
    else{
        res.render('auth/register', { csrfToken: req.csrfToken() });
    }
}

// Register User
exports.register_user = async (req,res) => {
    try {

        const val_errors = validationResult(req);
        if (!val_errors.isEmpty()) {
          return res.status(422).render('errors/422', {
              val_errors: val_errors
            });
        }
        
        // Check Email Exists
        let exists = Users.find({email: req.body.email});
        if(!exists){
            var salt = bcrypt.genSaltSync(10);
            var hash = bcrypt.hashSync(req.body.password, salt);
            let refresh_token = uuidv4();
    
            const User = new Users({
                name: req.body.name,
                phone: req.body.phone,
                email: req.body.email,
                password: hash,
                user_type: 'default',
                refresh_token: refresh_token,
                reset_password_requested: false,
                reset_password_token: null,
                reset_password_expiry: null,
                created_at: Date.now(),
                updated_at: Date.now()
            });
    
            await User.save((err,savedUser) => {
                if(!err){
                    let id = String(savedUser._id);
                    const token = jwt.sign(
                        {
                            _id: cookieEncrypt.encrypt(id)
                        },
                        process.env.JWT_APP_KEY,
                        {
                            expiresIn: process.env.REFRESH_JWT_INTERVAL
                        }
                        );
    
                        // Send Mail
                        if(process.env.MAIL_SERVICE && process.env.MAIL_FROM && process.env.MAIL_USER && process.env.MAIL_PASSWORD && savedUser.email){
                        let mailOptions = {
                            from: process.env.MAIL_FROM,
                            to: savedUser.email,
                            subject: 'Thank You For Registering With Doctor D',
                            html: '<h3>Hello ' + savedUser.name + ',</h3><p>You Have Successfully Registered On Doctor D.<br />Thanks</p>'
                          };
    
                        mail.sendMail(mailOptions, function(error, info){
                            if (error) {
                              console.log(error);
                            }
                          });
                        }
    
                        // Assign Cookies
                        Appointment.find({user_id: id}, function(err,appointment){
                            if(!err){
                                res.cookie('doctord_session', token, cookieConfig).cookie('doctord_refresh', cookieEncrypt.encrypt(refresh_token), cookieConfig)
                                .render('auth/dashboard', { 
                                    csrfToken:  req.csrfToken(),
                                    user_detail: savedUser,
                                    appointments: appointment
                                 });
                            }
                        });
                    }
            });
        }
        else{
            res.render('errors/messages', {val_errors: "This Email Is Already Taken."});
        }

    } catch (error) {
        res.render('errors/messages', {val_errors: error});
    }
}

// Login User Layout
exports.login = (req,res,next) => {
    if(req.signedCookies.doctord_session){
        res.redirect('/dashboard');
    }
    else{
        res.render('auth/login', { csrfToken: req.csrfToken() });
    }
}

// Login User
exports.login_user = async (req,res) => {

        const val_errors = validationResult(req);
        if (!val_errors.isEmpty()) {
          return res.status(422).render('errors/422', {
              val_errors: val_errors
            });
        }

        // Find User
        const user = await Users.findOne({email: req.body.email});
        if(!user) return res.status(400).render('errors/messages', {val_errors: 'Email Or Password Does Not Match.'});
        const validUser = await bcrypt.compare(req.body.password,user.password);
        if(!validUser) return res.status(400).render('errors/messages', {val_errors: 'Email Or Password Does Not Match.'});

        let id = String(user._id);
        let refresh_token = user.refresh_token;

        const token = jwt.sign(
            {
                _id: cookieEncrypt.encrypt(id)
            },
            process.env.JWT_APP_KEY,
            {
                expiresIn: process.env.REFRESH_JWT_INTERVAL
            }
            );
            // Check Appointment History And Display On Dashboard
            // Send The Cookies
            await Appointment.find({user_id: id}, function(err,appointment){
                if(!err){
                    res.cookie('doctord_session', token, cookieConfig).cookie('doctord_refresh', cookieEncrypt.encrypt(refresh_token), cookieConfig)
                    .render('auth/dashboard', { 
                        csrfToken:  req.csrfToken(),
                        user_detail: user,
                        appointments: appointment
                     });
                }
            });
        }

// Reset Password Layout
exports.password_reset = (req,res,next) => {
    // Check if user is logged in.
    if(req.signedCookies.doctord_session){
        res.redirect('/dashboard');
        next();
    }
    else{
        res.render('auth/password_reset', { csrfToken: req.csrfToken() });
    }
}

// User Enters Email And Confirms Reset Password
exports.password_reset_success = async (req,res,next) => {
    const val_errors = validationResult(req);
    if (!val_errors.isEmpty()) {
      return res.status(422).render('errors/422', {
          val_errors: val_errors
        });
    }

    // Find User
    const user = await Users.findOne({email: req.body.email});
    if(!user) return res.status(400).render('errors/messages', {val_errors: 'Email Does Not Exist.'});

    let hash = cookieEncrypt.encrypt(uuidv4());
    let expiry_date = new Date;
    // 1 Hour
    expiry_date.setHours(expiry_date.getHours() + 1);

    user.reset_password_requested = true;
    user.reset_password_token = hash;
    user.reset_password_expiry = expiry_date;
    user.updated_at = Date.now();

    user.save((err) => {
        if(err){
            res.send('Error: ' + err);
        }
    });

    // Send Mail
    if(process.env.MAIL_SERVICE && process.env.MAIL_FROM && process.env.MAIL_USER && process.env.MAIL_PASSWORD && user.email){
    let mailOptions = {
        from: process.env.MAIL_FROM,
        to: user.email,
        subject: 'Password Reset Request',
        html: '<h3>Hello ' + user.name + ',</h3><p>We Have Recieved A Password Reset Request On Your Behalf</p><br /><a target="_blank" href="'+process.env.APP_URL+'/reset/password/'+ hash +'">Reset My Password</a><br />Not You? You Can Ignore This E-mail. No Further Action Is Required.'
      };

    mail.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        }
      });
    }

    res.status(200).render('errors/messages', {val_errors:'A Password Reset Link Has Been Sent To Your Email.'});
}

// Check The Token And Render Password Form
exports.confirm_reset = async (req,res,next) => {

    const val_errors = validationResult(req.params);
    if (!val_errors.isEmpty()) {
      return res.status(422).render('errors/422', {
          val_errors: val_errors
        });
    }

    let token = req.params.token;
    let user_id = cookieEncrypt.decrypt(token);
    let user = await Users.findOne({reset_password_token: token});
    if(!user) return res.status(500).render('errors/500');
    if(user._id != user_id) return res.status(500).render('errors/500');
    if(user.reset_password_requested != true) return res.status(500).render('errors/500');
    if(user.reset_password_expiry < Date.now()) return res.status(500).render('errors/500');

    res.render('auth/change_password',{id: token, csrfToken: req.csrfToken()});
}

// Post Request Save The Password
exports.change_password = async (req,res,next) => {
    const val_errors = validationResult(req.params);
    if (!val_errors.isEmpty()) {
      return res.status(422).render('errors/422', {
          val_errors: val_errors
        });
    }

    id = cookieEncrypt.decrypt(req.body.id);
    let user = await Users.findOne({_id: id});
    if(!user) return res.status(500).render('errors/500');

    var salt = bcrypt.genSaltSync(10);
    var hash = bcrypt.hashSync(req.body.password, salt);

    user.password = hash;
    user.reset_password_requested = false;
    user.reset_password_token = null;
    user.reset_password_expiry = null;
    user.updated_at = Date.now();
    user.save();

    // Send Mail
    if(process.env.MAIL_SERVICE && process.env.MAIL_FROM && process.env.MAIL_USER && process.env.MAIL_PASSWORD){
        let mailOptions = {
            from: process.env.MAIL_FROM,
            to: user.email,
            subject: 'Your Password Has Been Reset',
            html: '<h3>Hello ' + user.name + ',</h3><p>You Have Successfully Changed Your Password.<br />Thanks.'
          };
    
        mail.sendMail(mailOptions, function(error, info){
            if (error) {
              console.log(error);
            }
          });
        }
    
    res.clearCookie('doctord_session').clearCookie('doctord_refresh').redirect('/');
}

// Logout
exports.logout = async (req,res,next) => {
    if(req.signedCookies.doctord_session){
        res.clearCookie('doctord_session');
    }
    if(req.signedCookies.doctord_refresh){
        res.clearCookie('doctord_refresh');
    }
    res.status(200).redirect('/');
}