const mongoose = require('mongoose');
// Controller Head
const AppointmentReq = require('../model/AppointmentReq');
const Users = require('../model/Users');
const Appointment = require('../model/Appointment');

const route = require('../route/app');

const csrf = require('csurf');
const csrfProtection = csrf({ cookie: true });

const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const parseForm = bodyParser.urlencoded({ extended: false });

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const crypto = require("crypto");

const cookieEncrypt = require('../helpers/cookieEncrypt');

const cookieConfig = {
    httpOnly: true,
    maxAge: 604800000,
    signed: true,
    sameSite: true
};

const mail = require('../helpers/mail');
const { check, validationResult } = require('express-validator');
const sa = require('superagent');

// Hit These Routes
exports.api = (req,res) => {
    res.json('API');
}

// Application Docs
exports.app_docs = (req,res) => {
    res.json('DOCS');
}

// HomePage
exports.index = (req,res) => {
    res.render('index', { csrfToken: req.csrfToken() });
};

// User Book Appointments
exports.book_appointment = async (req,res) => {
    let user_id;
    if(req.signedCookies.doctord_session){
        let token = req.signedCookies.doctord_session;
        await jwt.verify(token, process.env.JWT_APP_KEY, async function (err, decoded) {
            if (!err) {
                user_id = cookieEncrypt.decrypt(decoded._id);
                res.render('book_appointment', { csrfToken: req.csrfToken(), user_id: user_id });
            }
        });
    }
    else{
        res.render('book_appointment', { csrfToken: req.csrfToken(), user_id: false });
    }
};

// Post Request Save Appointment Request
exports.book_appointment_request = async (req,res) => {
    try {
        const val_errors = validationResult(req);
        if (!val_errors.isEmpty()) {
          return res.status(422).render('errors/422', {
              val_errors: val_errors
            });
        }

        const Appointment = new AppointmentReq({
            name: req.body.name,
            user_id: req.body.user_id,
            phone: req.body.phone,
            email: req.body.email,
            referer_doc: req.body.referer,
            appointment_type: req.body.type,
            created_at: Date.now(),
            updated_at: Date.now()
        });
        Appointment.save();

    // Send Mail
    if(process.env.MAIL_SERVICE && process.env.MAIL_FROM && process.env.MAIL_USER && process.env.MAIL_PASSWORD && req.body.email){
        let mailOptions = {
            from: process.env.MAIL_FROM,
            to: req.body.email,
            subject: 'Appointment Requested',
            html: '<h3>Hello ' + req.body.name + ',</h3><p>You Have Successfully Requested An Appointment.<br />One Of Our Staff Members Will Get In Touch With You Shortly For Appointment Date / Time.</p>'
          };

        mail.sendMail(mailOptions, function(error, info){
            if (error) {
              console.log(error);
            }
          });
        }
        if(process.env.BULKGATE_API_URL && process.env.BULKGATE_APP_ID && process.env.BULKGATE_API_KEY && req.body.phone){
          sa.post(process.env.BULKGATE_API_URL)
          .send({
            "application_id": process.env.BULKGATE_APP_ID, 
            "application_token": process.env.BULKGATE_API_KEY,
            "number": req.body.phone,
            "text": "Hello " + req.body.name + ", You Have Successfully Requested An Appointment. One Of Our Staff Members Will Get In Touch With You Shortly For Appointment Date / Time.",
            "country": process.env.BULKGATE_COUNTRY_CODE
          })
          .end(function(err, result) {
            if(err) console.log(err);
          });
        }

        res.render('booked', { csrfToken: req.csrfToken() });
        
    } catch (error) {
        res.render('errors/messages', {val_errors: error});
    }
};