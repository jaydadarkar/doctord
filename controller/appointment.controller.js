const mongoose = require('mongoose');
const route = require('../route/app');
const csrf = require('csurf');
const csrfProtection = csrf({ cookie: true });
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const parseForm = bodyParser.urlencoded({ extended: false });
const Users = require('../model/Users');
const Appointments = require('../model/Appointment');
const { check, validationResult } = require('express-validator');
const mail = require('../helpers/mail');
const sa = require('superagent');

// Create Appointment Layout
exports.create = async (req,res,next) => {
    res.render('admin/create', { csrfToken: req.csrfToken() });
}

// Save Appointment
exports.store = async (req,res,next) => {
    try {
        let val_errors = validationResult(req);
        if (!val_errors.isEmpty()) {
          return res.status(422).render('errors/422', {
              val_errors: val_errors
            });
        }

        let user = await Users.findById(req.body.user_id);
        if(!user) {
            return res.render('errors/messages', { val_errors: 'Patient Not Found...' });
        }

        // Declare constant as we don't want to change the value in this scope.
        // Initialize Model
        const Appointment = new Appointments({
            user_id: req.body.user_id,
            appointment_type: req.body.appointment_type,
            appointment_date_time: req.body.appointment_date_time,
            prescription: null,
            created_at: Date.now(),
            updated_at: Date.now()
        });

        await Appointment.save((err,savedUser) => {
            if(!err){

                // Send Email
                if(process.env.MAIL_SERVICE && process.env.MAIL_FROM && process.env.MAIL_USER && process.env.MAIL_PASSWORD && user.email){
                    let mailOptions = {
                        from: process.env.MAIL_FROM,
                        to: user.email,
                        subject: 'Appointment Booked',
                        html: '<h3>Hello ' + user.name + ',</h3><p>An Appoinment Has Been Booked. <br />Date / Time: ' + req.body.appointment_date_time + '<br />Please Reach 15 Minutes Before Time.<br />Thanks.</p>'
                    };
    
                    mail.sendMail(mailOptions, function(error, info){
                        if (error) {
                          console.log(error);
                        }
                    });
                }

                // Send SMS
                if(process.env.BULKGATE_API_URL && process.env.BULKGATE_APP_ID && process.env.BULKGATE_API_KEY && user.phone){
                sa.post(process.env.BULKGATE_API_URL)
                .send({
                  "application_id": process.env.BULKGATE_APP_ID, 
                  "application_token": process.env.BULKGATE_API_KEY,
                  "number": user.phone,
                  "text": "Hello " + user.name + ", Your Appointment Is Scheduled At: " + req.body.appointment_date_time + ". Please Reach 15 Minutes Before Time. Thanks.",
                  "country": process.env.BULKGATE_COUNTRY_CODE
                })
                .end(function(err, result) {
                  if(err) console.log(err);
                });
                }

                res.render('errors/messages', { val_errors: 'Appointment Booked...' });
            }
        });
    } catch (error) {
        res.render('errors/messages', {val_errors: error});
    }

}

// Get All Appointments
exports.read = async (req,res,next) => {

    await Appointments.find({}, function(err,Appointment){
        if(!err){
            res.render('admin/viewAppointments', {csrfToken: req.csrfToken(), appointments: Appointment});
        }
        else{
            res.render('errors/messages', {val_errors: err});
        }
    }).sort({ created_at: -1 });    
}

// View One Appointment
exports.viewOne = async (req,res,next) => {

    let val_errors = validationResult(req.params.id);
    if (!val_errors.isEmpty()) {
      return res.status(422).render('errors/422', {
          val_errors: val_errors
        });
    }

    let appointment_id = String(req.params.id);
    await Appointments.findOne({_id: appointment_id}, async function (err, appointment){
        if(!err){
            // Get History Of Patient
            history = await Appointments.find({user_id: appointment.user_id}).sort({ created_at: -1 });
            res.render('admin/viewOne', {csrfToken: req.csrfToken(), appointment: appointment, history: history});
        }
        else{
            res.render('errors/messages', {val_errors: err});
        }
    });
}

// Update Prescription Only
exports.update = async (req,res,next) => {
    let appointment_id = String(req.body.appointment_id);
    await Appointments.findOne({_id: appointment_id}, async function (err, appointment){
        if(!err){
            let user = await Users.findOne({_id: appointment.user_id});
            await Appointments.updateOne({_id: appointment_id},{
                prescription: req.body.prescription,
                updated_at: Date.now()
            },function(err){
                if(err){
                    res.render('errors/messages', {val_errors: err});
                }
                else{

                    // Send Mail
                    if(process.env.MAIL_SERVICE && process.env.MAIL_FROM && process.env.MAIL_USER && process.env.MAIL_PASSWORD && user.email){
                        let mailOptions = {
                        from: process.env.MAIL_FROM,
                        to: user.email,
                        subject: 'Appointment Updated',
                        html: '<h3>Hello '+ user.name +',</h3><p>Your Appoinment Has Been Updated. <br />Prescription: </p>' + req.body.prescription
                      };
    
                    mail.sendMail(mailOptions, function(error, info){
                        if (error) {
                          console.log(error);
                        }
                      });
                    }

                    // Send SMS
                    if(process.env.BULKGATE_API_URL && process.env.BULKGATE_APP_ID && process.env.BULKGATE_API_KEY && user.phone){
                      sa.post(process.env.BULKGATE_API_URL)
                      .send({
                        "application_id": process.env.BULKGATE_APP_ID, 
                        "application_token": process.env.BULKGATE_API_KEY,
                        "number": user.phone,
                        "text": "Hello " + user.name + ", Your Appointment Has Been Updated. Prescription: "+ req.body.prescription.substring(0, 40) +"... Thanks.",
                        "country": process.env.BULKGATE_COUNTRY_CODE
                      })
                      .end(function(err, result) {
                        if(err) console.log(err);
                      });
                    }

                    res.render('errors/messages', {val_errors: 'Appointment Updated...'});
                }
            });
        }
        else{
            res.render('errors/messages', {val_errors: err});
        }
    });
}

// Get Today's Appointments
exports.today = async (req,res,next) => {
    let today_date = new Date().toISOString().slice(0, 10);
    app_today = await Appointments.find({'$where': 'this.appointment_date_time.toJSON().slice(0, 10) == "'+ today_date +'"'});
    res.render('admin/today', {csrfToken: req.csrfToken(), today: app_today});
}

// Optional But Not Recomended
exports.delete = async (req,res,next) => {

}