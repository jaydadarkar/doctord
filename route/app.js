// Route Head
const express = require('express');
const router = express.Router();
const csrf = require('csurf');
const csrfProtection = csrf({ cookie: true });
const cookieParser = require('cookie-parser');
bodyParser = require('body-parser');
parseForm = bodyParser.urlencoded({ extended: false });
const { check, validationResult } = require('express-validator');

// Middlewares
const auth = require('../helpers/auth');
const isAdmin = require('../helpers/isAdmin');

// Controllers
const controller = require('../controller/controller');
const auth_controller = require('../controller/auth.controller');
const admin_controller = require('../controller/admin.controller');
const appointment = require('../controller/appointment.controller');

// No CSRF Protection
router.get('/api/', controller.api);
router.get('/application-docs/', controller.app_docs);

// CSRF Protection
router.get('/', csrfProtection, controller.index);

// Auth
router.get('/register/', csrfProtection, auth_controller.register);
router.post('/register/', parseForm, csrfProtection, [check('name').isString(), check('email').isEmail(), check('phone').isString(),check('phone').isLength({ min: 10, max: 10 }),
                                                      check('password').isString(), check('password').isLength({ min: 8 })], auth_controller.register_user);
router.get('/login/', csrfProtection, auth_controller.login);
router.post('/login/', parseForm, csrfProtection, [check('email').isEmail(),check('password').isString(), check('password').isLength({ min: 8 })],
                                                     auth_controller.login_user);
router.get('/password/reset', parseForm, csrfProtection, auth_controller.password_reset);
router.post('/password/reset', parseForm, csrfProtection, [check('email').isEmail()], auth_controller.password_reset_success);
router.get('/logout/', parseForm, csrfProtection, auth_controller.logout);
router.get('/reset/password/:token',parseForm, csrfProtection, [check('token').isString()],auth_controller.confirm_reset);
router.post('/password/change/', parseForm, csrfProtection, [check('id').isString(),check('password').isString(), check('password').isLength({ min: 8 })], auth_controller.change_password);

// User Dashboard
router.get('/dashboard/', [parseForm, csrfProtection, auth], auth_controller.dashboard);
// Request Appointments
router.get('/book-appointment/', parseForm, csrfProtection, controller.book_appointment);
router.post('/book-appointment/request/', parseForm, csrfProtection, [check('name').isString(), check('email').isEmail(), check('phone').isString(),
check('user_id').isString(), check('referer').isString(), check('type').isString()], controller.book_appointment_request);

// Admin
router.get('/admin/', parseForm, csrfProtection, isAdmin, admin_controller.admin);
router.get('/admin/users', parseForm, csrfProtection, isAdmin, admin_controller.users);
router.get('/admin/user/:id', parseForm, csrfProtection, [check('id').isString()], isAdmin, admin_controller.user);
router.post('/admin/user/update/', parseForm, csrfProtection, isAdmin, admin_controller.updateUser);
router.get('/admin/appointment/create', parseForm, csrfProtection, isAdmin, appointment.create);
router.post('/admin/appointment/create/store', parseForm, csrfProtection, isAdmin, appointment.store);
router.get('/admin/appointment/view', parseForm, csrfProtection, isAdmin, appointment.read);
router.get('/admin/appointment/today', parseForm, csrfProtection, isAdmin, appointment.today);
router.get('/admin/appointment/update/:id', parseForm, csrfProtection, [check('id').isString()], isAdmin, appointment.viewOne);
router.post('/admin/appointment/update/', parseForm, csrfProtection, isAdmin, appointment.update);

// 404 Page
router.get('*', parseForm, csrfProtection, function(req, res){
    res.status(404).render('errors/404', { csrfToken: req.csrfToken() });
  });

module.exports = router;